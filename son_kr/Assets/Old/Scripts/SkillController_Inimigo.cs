﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillController_Inimigo : MonoBehaviour
{
    public float raio;
    public float alcance;

    public enum SkillType { Ataque, Cura };
    public SkillType tipoSkill;

    //Atributo primário é o atributo utilizado para calcular o que gasta e o resultado final do ataque/cura
    //Ex.: 
    //      1) Uma skill de ataque que usa força, gastará stamina e terá o atributo força utilizado no cálculo do dano
    //      2) Uma skill de ataque que usa destreza, gastará stamina e terá o atributo destreza utilizado no cálculo do dano
    //      3) Uma skill de cura que usa inteligêcia, gastará mana e terá o atributo inteligência utilizado no cálculo da cura
    public enum AtributoPrimario { Forca, Destreza, Inteligencia };
    public AtributoPrimario atributoPrimario;

    //Valor base será o valor do dano/cura
    public int valorBase;

    //Custo da skill
    //Gastará mana ou stamina
    public int custo;

    public float tempo;

    public Inimigo usuario;

    Vector3 alvo;

    public enum SkillPattern { ParadoHorizontal, ParadoVertical, ParadoEmArea, LancavelFinito, LancavelInfinito, Posicionavel, Estocada };
    public SkillPattern padrao;
    //7 padrões de comportamento:
    //  1 - Horizontal parado;
    //  2 - Vertical parado;
    //  3 - Em área parado (área de acordo com o raio)
    //  4 - Lançável com alcance > 0 (vai terminar depois que se movimentar um pouco) (com raio ou sem raio)
    //  5 - Lançável com alcance = 0 (vai terminar depois que alcançar o máximo do alcance permitido) (com raio ou sem raio)
    //  6 - Posicionável com ou sem raio (Faz a skill acontecer instantaneamente na área selecionada)
    //  7 - Estocada (tipo uma lança)


    //private BoxCollider2D colisor;

    // Start is called before the first frame update
    void Start()
    {
        //colisor = GetComponent<BoxCollider2D>();
        switch (padrao)
        {
            case SkillPattern.ParadoHorizontal:
                Horizontal2();
                break;
            case SkillPattern.ParadoVertical:
                Vertical();
                break;
            case SkillPattern.Estocada:
                Estocada();
                break;
            case SkillPattern.ParadoEmArea:
                ParadoEmArea();
                break;
            case SkillPattern.LancavelInfinito:
                LancavelInfinito();
                break;
            case SkillPattern.Posicionavel:
                Posicionavel();
                break;
            default:
                Vertical();
                break;
        }
    }

    private void LancavelInfinito()
    {
        Debug.Log("Usou ataque lancavel infinito");
        alvo = CalculaAlvo(gameObject.transform.position);
        switch (usuario.DirecaoInt)
        {
            case 1:
                alvo.y -= 15f;
                break;
            case 2: //Nas diagonais, como aqui, teria que ser 15 * seno 45
                alvo.x -= 15f * 0.7071068f;
                alvo.y -= 15f * 0.7071068f;
                break;
            case 3:
                alvo.x -= 15f;
                break;
            case 4:
                alvo.x -= 15f * 0.7071068f;
                alvo.y += 15f * 0.7071068f;
                break;
            case 5:
                alvo.y += 15f;
                break;
            case 6:
                alvo.x += 15f * 0.7071068f;
                alvo.y += 15f * 0.7071068f;
                break;
            case 7:
                alvo.x += 15f;
                break;
            case 8:
                alvo.x += 15f * 0.7071068f;
                alvo.y += 15f * 0.7071068f;
                break;
            default:
                break;
        }

        //Vector3.MoveTowards(gameObject.transform.position, alvo, Time.deltaTime);
        LeanTween.move(gameObject, alvo, tempo).setOnComplete(Destroi);
    }

    private void ParadoEmArea()
    {
        Debug.Log("Usou ataque Parado em Área");
        LeanTween.move(gameObject, usuario.Posicao, 0.0001f).setOnComplete(() =>
        {
            LeanTween.scale(gameObject, new Vector3(raio, raio, raio), tempo).setOnComplete(Destroi);
        });
    }

    public void Vertical()
    {
        Debug.Log("Usou Ataque Vertical");
        LeanTween.rotateZ(this.gameObject, 150, .3f).setOnComplete(Destroi);
    }

    public void Horizontal()
    {
        Debug.Log("Usou Ataque Horizontal");

        switch (usuario.DirecaoInt)
        {
            case 1:
                LeanTween.rotateZ(gameObject, 120, 0.0001f).setOnComplete(() =>
                {
                    LeanTween.rotateZ(gameObject, -120, 0.3f).setOnComplete(Destroi);
                });
                break;
            case 2:
                break;
            case 3:
                //Implementar essa parte
                LeanTween.rotateZ(gameObject, 45, 0.0001f).setOnComplete(() =>
                {
                    LeanTween.rotateZ(gameObject, 120, 0.3f).setOnComplete(Destroi);
                });
                break;
            case 4:
                //Implementar essa parte
                break;
            case 5:
                LeanTween.rotateZ(gameObject, -45, 0.0001f).setOnComplete(() =>
                {
                    LeanTween.rotateZ(gameObject, 45, 0.3f).setOnComplete(Destroi);
                });
                break;
            case 6:
                //Implementar essa parte
                break;
            case 7:
                LeanTween.rotateZ(gameObject, -120, 0.0001f).setOnComplete(() =>
                {
                    LeanTween.rotateZ(gameObject, -45, 0.3f).setOnComplete(Destroi);
                });
                break;
            case 8:
                //Implementar essa parte
                break;
            default:
                break;
        }
        //alvo = CalculaAlvo(this.transform.position);
        //gameObject.GetComponent<RectTransform>().pivot = alvo;
        //gameObject.GetComponent<SpriteRenderer>().s
        //LeanTween.rotateZ(gameObject, -130, 0.5f).setOnComplete(Destroi);
        //gameObject.GetComponent<RectTransform>().pivot = new Vector2(mousePosition.x, mousePosition.y);

    }

    public void Posicionavel()
    {
        LeanTween.scale(gameObject, new Vector3(1, 1, 1), 0.3f);
    }

    public void Horizontal2()
    {
        Debug.Log("Usou Ataque Horizontal2");
        //Aumenta o tamanho do quadrado para uma "barra"
        /*//verifica a direcao de olhar do personagem
        if (jogador.Direcao.x < 0) alvo.x = jogador.Posicao.x - 0.5f;
        if (jogador.Direcao.x == 0) alvo.x = jogador.Posicao.x;
        if (jogador.Direcao.x > 0) alvo.x = jogador.Posicao.x + 0.5f;
        if (jogador.Direcao.y < 0) alvo.y = jogador.Posicao.y - 0.5f;
        if (jogador.Direcao.y == 0) alvo.y = jogador.Posicao.y;
        if (jogador.Direcao.y > 0) alvo.y = jogador.Posicao.y + 0.5f;
        */


        Vector3 scale = new Vector3();
        scale.x = 1f;
        scale.y = 0.1f;
        scale.z = 1f;

        //O pivot e movimentações mudarão conforme a direção do ator

        LeanTween.rotateZ(this.gameObject, 60f, 0.0001f).setOnComplete(() => {
            LeanTween.scale(this.gameObject, scale, 0.0001f).setOnComplete(() => {
                LeanTween.moveX(this.gameObject, -0.1f, 0.0001f).setOnComplete(() => {
                    LeanTween.rotateAround(this.gameObject, Vector3.back, 120f, 0.5f).setOnComplete(Destroi);
                });
            });
        });
    }

    public void Estocada()
    {
        Debug.Log("Usou estocada");
        alvo = CalculaAlvo(gameObject.transform.position);
        LeanTween.move(gameObject, alvo, tempo).setOnComplete(Destroi);
        //verificar necessidade de fazer a animação de voltar a estocada
    }

    public Vector3 CalculaAlvo(Vector3 origem)
    {
        Vector3 destino = origem;
        // 4-5-6
        // 3-X-7
        // 2-1-8
        switch (usuario.DirecaoInt)
        {
            case 1:
                destino.y -= alcance;
                break;
            case 2:
                destino.x -= alcance * 0.7071068f;
                destino.y -= alcance * 0.7071068f;
                break;
            case 3:
                destino.x -= alcance;
                break;
            case 4:
                destino.x -= alcance * 0.7071068f;
                destino.y += alcance * 0.7071068f;
                break;
            case 5:
                destino.y += alcance;
                break;
            case 6:
                destino.x += alcance * 0.7071068f;
                destino.y += alcance * 0.7071068f;
                break;
            case 7:
                destino.x += alcance;
                break;
            case 8:
                destino.x += alcance * 0.7071068f;
                destino.y -= alcance * 0.7071068f;
                break;
            default:
                break;
        }
        return destino;
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Acertou o " + collision);
        ExecutarSkill(collision);
    }

    void ExecutarSkill(Collider2D collision)
    {
        CharController controladorJogador = collision.gameObject.GetComponent<CharController>();
        //Inimigo controladorInimigo = collision.gameObject.GetComponent<Inimigo>();
            //if (tipoSkill == SkillType.Ataque && controladorJogador != null)
            if (controladorJogador != null)
            {
                //É ataque e acertou o jogador, executa..
                int testeJogador = Random.Range(1, 20);
                int testeUsuario = Random.Range(1, 20);

                switch (atributoPrimario)
                {
                    case AtributoPrimario.Destreza:
                    case AtributoPrimario.Forca:
                        testeJogador += controladorJogador.ator.Destreza;
                        testeUsuario += usuario.Destreza;
                        break;
                    case AtributoPrimario.Inteligencia:
                        testeJogador += controladorJogador.ator.Inteligencia;
                        testeUsuario += usuario.Inteligencia;
                        break;
                }

                if (testeJogador > 20)
                {
                    testeJogador = 20;
                }

                if (testeUsuario > 20)
                {
                    testeUsuario = 20;
                }

                if (testeJogador < testeUsuario)
                {
                    //Se o ator ganhou no teste, acertou, calcula o dano
                    //O dano será dano da arma + 1dX (valor base) + atributo primário da skill
                    int dano = Random.Range(1, 4); //Simulando 1d4 para arma
                    dano += Random.Range(1, valorBase); //Valor base da skill

                    switch (atributoPrimario)
                    {
                        case AtributoPrimario.Destreza:
                            dano += usuario.Destreza;
                            break;
                        case AtributoPrimario.Forca:
                            dano += usuario.Forca;
                            break;
                        case AtributoPrimario.Inteligencia:
                            dano += usuario.Inteligencia;
                            break;
                    }

                    //Esse dano ainda poderá ser absorvido totalmente, dependendo da constituição do inimigo
                    //A princípio faz apenas a diferença entre o dano e a força do inimigo
                    dano = dano - controladorJogador.ator.Forca;

                    //Se o dano final for menor do que 0, significa que absorveu o dano totalmente
                    if (dano <= 0)
                    {
                    controladorJogador.DisplayBlock();
                    }
                    else
                    {
                    //Pegou dano
                    controladorJogador.DisplayHit(dano);
                    }
                }
                else
                {
                //Ator perdeu no teste, esquiva
                controladorJogador.DisplayDodge();
                }
            }
    }


    /// <summary>
    /// Destrói a skill
    /// </summary>
    public void Destroi()
    {
        Destroy(gameObject);
    }
}
