﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class UI_Atualiza : MonoBehaviour
{
    [SerializeField]
    private Ator jogador;

    [SerializeField]
    private TMP_Text vida, mana, stamina;

    [SerializeField]
    private GameObject healthBarObject, manaBarObject, staminaBarObject;

    private Image healthBar, manaBar, staminaBar;

    void Start()
    {
        vida.SetText(jogador.PontosDeVidaAtuais + " / " + jogador.PontosDeVidaTotais);
        mana.SetText(jogador.PontosDeManaAtuais + " / " + jogador.PontosDeManaTotais);
        stamina.SetText(jogador.PontosDeStaminaAtuais + " / " + jogador.PontosDeStaminaTotais);

        healthBar = healthBarObject.GetComponent<Image>();
        healthBar.fillAmount = (float) jogador.PontosDeVidaAtuais / (float) jogador.PontosDeVidaTotais;

        manaBar = manaBarObject.GetComponent<Image>();
        manaBar.fillAmount = (float) jogador.PontosDeManaAtuais / (float) jogador.PontosDeManaTotais;

        staminaBar = staminaBarObject.GetComponent<Image>();
        staminaBar.fillAmount = (float)jogador.PontosDeStaminaAtuais / (float)jogador.PontosDeStaminaTotais;
    }

    void Update()
    {
        vida.SetText(jogador.PontosDeVidaAtuais + " / " + jogador.PontosDeVidaTotais);
        healthBar.fillAmount = (float)jogador.PontosDeVidaAtuais / (float)jogador.PontosDeVidaTotais;

        mana.SetText(jogador.PontosDeManaAtuais + " / " + jogador.PontosDeManaTotais);
        manaBar.fillAmount = (float)jogador.PontosDeManaAtuais / (float)jogador.PontosDeManaTotais;

        stamina.SetText(jogador.PontosDeStaminaAtuais + " / " + jogador.PontosDeStaminaTotais);
        staminaBar.fillAmount = (float)jogador.PontosDeStaminaAtuais / (float)jogador.PontosDeStaminaTotais;
    }
}
