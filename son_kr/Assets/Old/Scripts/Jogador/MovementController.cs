﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.Animations;
using UnityEngine;

public class MovementController : MonoBehaviour
{
    private CharController controladorAtor;
    private Rigidbody2D corpoRigido;
    private BoxCollider2D colisor;
    private Animator animador;

    float h;
    float v;

    float maxTimeOnDirection = 0.5f;
    float timeOnDirection;
    int direction;

    public Joystick joystick;

    // Start is called before the first frame update
    void Start()
    {
        corpoRigido = GetComponent<Rigidbody2D>();
        animador = GetComponent<Animator>();
        controladorAtor = GetComponent<CharController>();

        timeOnDirection = maxTimeOnDirection;
        direction = 0;
    }

    // Update is called once per frame
    void Update()
    {
        h = Input.GetAxis("Horizontal");
        if (joystick.Horizontal != 0) h = joystick.Horizontal;
        v = Input.GetAxis("Vertical");
        if (joystick.Vertical != 0) v = joystick.Vertical;

        controladorAtor.ator.Deslocamento.x = h;
        controladorAtor.ator.Deslocamento.y = v;

        if ((!Mathf.Approximately(controladorAtor.ator.Deslocamento.x, 0.0f)) || (!Mathf.Approximately(controladorAtor.ator.Deslocamento.y, 0.0f)))
        {
            controladorAtor.ator.Direcao.Set(controladorAtor.ator.Deslocamento.x, controladorAtor.ator.Deslocamento.y, 0);
            controladorAtor.ator.Direcao.Normalize();
        }


        //Atualiza a variavel int da direcao do olhar do autor
        // 3-4-5
        // 2-X-6
        // 1-0-7
        if (controladorAtor.ator.Direcao.x == 0) //Olhando reto para cima ou para baixo 
        {
            if (controladorAtor.ator.Direcao.y < 0) controladorAtor.ator.DirecaoInt = 0; //Baixo
            else controladorAtor.ator.DirecaoInt = 4; //Cima
        }
        else
        {
            if (controladorAtor.ator.Direcao.x < 0) //Olhando para esquerda baixo, esquerda ou esquerda cima
            {
                if (controladorAtor.ator.Direcao.y < 0) controladorAtor.ator.DirecaoInt = 1; //esquerda baixo
                else
                {
                    if (controladorAtor.ator.Direcao.y == 0) controladorAtor.ator.DirecaoInt = 2; //esquerda
                    else controladorAtor.ator.DirecaoInt = 3; //esquerda cima
                }
            }
            else //Olhando para direita cima, direita ou direita baixo
            {
                if (controladorAtor.ator.Direcao.y > 0) controladorAtor.ator.DirecaoInt = 5; //direita cima
                else
                {
                    if (controladorAtor.ator.Direcao.y == 0) controladorAtor.ator.DirecaoInt = 6; //direita
                    else controladorAtor.ator.DirecaoInt = 7; //direita baixo
                }
            }
        }

        if (animador != null)
        {
            animador.SetFloat("Horizontal", controladorAtor.ator.Direcao.x);
            animador.SetFloat("Vertical", controladorAtor.ator.Direcao.y);
            animador.SetFloat("Velocidade", controladorAtor.ator.Deslocamento.magnitude);
        }

        controladorAtor.ator.Posicao.x = corpoRigido.position.x + controladorAtor.ator.Deslocamento.x * controladorAtor.ator.Destreza * 0.5f * Time.fixedDeltaTime;
        controladorAtor.ator.Posicao.y = corpoRigido.position.y + controladorAtor.ator.Deslocamento.y * controladorAtor.ator.Destreza * 0.5f * Time.fixedDeltaTime;

        corpoRigido.MovePosition(controladorAtor.ator.Posicao);
    }
}
