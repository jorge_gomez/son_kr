﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Novo Ator", menuName = "Novo Ator")]
public class Ator : ScriptableObject
{
    public string Nome;

    //Força
    [Header("Força")]
    public int Forca;

    //Destreza
    [Header("Destreza")]
    public int Destreza;

    //Inteligência
    [Header("Inteligência")]
    public int Inteligencia;

    //Carisma ainda não tem aplicação

    //Pontos de Vida
    [Header("Pontos De Vida")]
    public int PontosDeVidaAtuais;
    public int PontosDeVidaTotais;

    //Pontos de Mana
    [Header("Pontos De Mana")]
    public int PontosDeManaAtuais;
    public int PontosDeManaTotais;

    //Pontos de Stamina
    [Header("Pontos de Stamina")]
    public int PontosDeStaminaAtuais;
    public int PontosDeStaminaTotais;

    [Header("Física")]
    public Vector3 Posicao;
    public Vector3 Direcao, Deslocamento;
    public int DirecaoInt;

}