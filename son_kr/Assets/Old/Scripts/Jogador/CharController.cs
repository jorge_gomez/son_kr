﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CharController : MonoBehaviour
{
    public Ator ator;

    [Header("Texto do Cabeçalho")]
    public TextMeshProUGUI headerText;

    public GameObject[] skillsAtivas = new GameObject[5];

    // Start is called before the first frame update
    void Start()
    {
        ator.Forca = 3;
        ator.Destreza = 3;
        ator.Inteligencia = 1;

        ator.PontosDeManaTotais = 100;
        ator.PontosDeManaAtuais = 100;
        ator.PontosDeVidaTotais = 100;
        ator.PontosDeVidaAtuais = 100;

        ator.Posicao = transform.position;
    }

    public Transform GetTransform()
    {
        return transform;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Atualiza()
    {
        //Vamos rever todo esse esquema de "Recém Atacou" e "Folego". Por isso removi esses atributos do ator.
        //Me parece que o ideal é fazer esse controle pela própria skill, verificando o cooldown e se tem mana/stamina para executar..
    }

    public void DisplayCriticalHit(int valor)
    {
        TextMeshProUGUI headerTextInstance = Instantiate(headerText, Vector3.up * 0.3f, Quaternion.identity);
        Transform canvasTransform = transform.Find("DynamicCanvas");
        headerTextInstance.transform.SetParent(canvasTransform, false);
        HeaderTextDisplay htd = headerTextInstance.GetComponent<HeaderTextDisplay>();
        htd.ConfigDisplayCriticalHit(valor.ToString());

        ator.PontosDeVidaAtuais -= valor;
        if (ator.PontosDeVidaAtuais >= ator.PontosDeVidaTotais) ator.PontosDeVidaAtuais = ator.PontosDeVidaTotais;
        if (ator.PontosDeVidaAtuais <= 0) Destroy(this.gameObject);
    }

    public void DisplayHit(int valor)
    {
        TextMeshProUGUI headerTextInstance = Instantiate(headerText, Vector3.up * 0.3f, Quaternion.identity);
        Transform canvasTransform = transform.Find("DynamicCanvas");
        headerTextInstance.transform.SetParent(canvasTransform, false);
        HeaderTextDisplay htd = headerTextInstance.GetComponent<HeaderTextDisplay>();
        htd.ConfigDisplayHit(valor.ToString());

        ator.PontosDeVidaAtuais -= valor;
        if (ator.PontosDeVidaAtuais >= ator.PontosDeVidaTotais) ator.PontosDeVidaAtuais = ator.PontosDeVidaTotais;
        if (ator.PontosDeVidaAtuais <= 0) Destroy(this.gameObject);
    }

    public void DisplayLife(int valor)
    {
        TextMeshProUGUI headerTextInstance = Instantiate(headerText, Vector3.up * 0.3f, Quaternion.identity);
        Transform canvasTransform = transform.Find("DynamicCanvas");
        headerTextInstance.transform.SetParent(canvasTransform, false);
        HeaderTextDisplay htd = headerTextInstance.GetComponent<HeaderTextDisplay>();
        htd.ConfigDisplayLife(valor.ToString());

        ator.PontosDeVidaAtuais += valor;
        if (ator.PontosDeVidaAtuais >= ator.PontosDeVidaTotais) ator.PontosDeVidaAtuais = ator.PontosDeVidaTotais;
        if (ator.PontosDeVidaAtuais <= 0) Destroy(this.gameObject);
    }

    public void DisplayBlock()
    {
        TextMeshProUGUI headerTextInstance = Instantiate(headerText, Vector3.up * 0.35f, Quaternion.identity);
        Transform canvasTransform = transform.Find("DynamicCanvas");
        headerTextInstance.transform.SetParent(canvasTransform, false);
        HeaderTextDisplay htd = headerTextInstance.GetComponent<HeaderTextDisplay>();
        htd.ConfigDisplayBlock("BLOQUEIO!");
    }

    public void DisplayDodge()
    {
        TextMeshProUGUI headerTextInstance = Instantiate(headerText, Vector3.up * 0.35f, Quaternion.identity);
        Transform canvasTransform = transform.Find("DynamicCanvas");
        headerTextInstance.transform.SetParent(canvasTransform, false);
        HeaderTextDisplay htd = headerTextInstance.GetComponent<HeaderTextDisplay>();
        htd.ConfigDisplayDodge("ESQUIVA!");
    }

    public void UsarSkill(int skillIndex)
    {
        GameObject skill = skillsAtivas[skillIndex];
        SkillController skillController = skill.GetComponent<SkillController>();
        skillController.controladorJogador = this;

        Vector3 lugarPraInstanciar = new Vector3();
        //Verifica pra onde o ator está ollhando
        if (ator.Direcao.x < 0) lugarPraInstanciar.x = ator.Posicao.x - skillController.alcance;
        if (ator.Direcao.x == 0) lugarPraInstanciar.x = ator.Posicao.x;
        if (ator.Direcao.x > 0) lugarPraInstanciar.x = ator.Posicao.x + skillController.alcance;
        if (ator.Direcao.y < 0) lugarPraInstanciar.y = ator.Posicao.y - skillController.alcance;
        if (ator.Direcao.y == 0) lugarPraInstanciar.y = ator.Posicao.y;
        if (ator.Direcao.y > 0) lugarPraInstanciar.y = ator.Posicao.y + skillController.alcance;

        Instantiate(skill, lugarPraInstanciar, Quaternion.identity);
    }
}
