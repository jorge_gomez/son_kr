﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using TMPro;
using UnityEngine;
using Pathfinding;

public class Inimigo : MonoBehaviour
{
    public string Nome;

    //Força
    [Header("Força")]
    public int Forca;

    //Destreza
    [Header("Destreza")]
    public int Destreza;

    //Inteligência
    [Header("Inteligência")]
    public int Inteligencia;

    //Carisma ainda não tem aplicação

    //Pontos de Vida
    [Header("Pontos De Vida")]
    public int PontosDeVidaAtuais;
    public int PontosDeVidaTotais;

    //Pontos de Mana
    [Header("Pontos De Mana")]
    public int PontosDeManaAtuais;
    public int PontosDeManaTotais;

    public Ator jogador;
    public Animator animador;

    [Header("Combate")]
    public float RaioAtaquePerto = 1.0f;
    public float RaioAtaqueLonge = 3.0f;
    public bool EmPosicaoPraAtacar = false;
    public GameObject[] skillsAtivas = new GameObject[5];
    public float folego, folegoMaximo;
    public bool PodeAtacar = true;

    [Header("Física")]
    public Vector3 Posicao;
    public Vector3 Direcao, Deslocamento;
    public int DirecaoInt;
    private float TempoTimer;
    public float TempoNaDirecao = 2.0f;
    public Rigidbody2D CorpoRigido;
    //public RaycastHit2D RaioVisao;
    //public RaycastHit2D[] RaioVisao = new RaycastHit2D[];

    [Header("Texto do Cabeçalho")]
    public TextMeshProUGUI headerText;

    public BoxCollider2D colisor2d;

    private void Start()
    {
        Posicao = transform.position;
        animador = GetComponent<Animator>();
        TempoTimer = TempoNaDirecao;
        if (folegoMaximo == 0) folegoMaximo = 1.0f;
        folego = folegoMaximo;
    }

    private void Update()
    {
        Movimenta();
        Atualiza();
    }


    public void Movimenta()
    {
        //Calcula a distância do inimigo ao jogador
        float Distancia = Vector3.Distance(Posicao, jogador.Posicao);

        if (Distancia > RaioAtaqueLonge)
        {
            //Jogador está fora do RaioAtaqueLonge, então movimenta o inimigo aleatoriamente
            EmPosicaoPraAtacar = false;
            Debug.Log(gameObject.name + " está em movimento aleatório");
            //Primeiro, escolhe uma direção pra seguir
            if (TempoTimer >= 0)
            {
                TempoTimer -= Time.deltaTime;
            }
            else
            {
                TempoTimer = TempoNaDirecao;
                int escolheDirecao = Random.Range(0, 8);
                switch (escolheDirecao)
                {
                    case 0: //Mover para baixo
                        Deslocamento.x = 0.0f;
                        Deslocamento.y -= 1.0f;
                        break;
                    case 1: //Mover para baixo esquerda
                        Deslocamento.x -= 1.0f * 0.7071068f;
                        Deslocamento.y -= 1.0f * 0.7071068f;
                        break;
                    case 2: //Mover para esquerda
                        Deslocamento.x -= 1.0f;
                        Deslocamento.y = 0.0f;
                        break;
                    case 3: //Mover para cima esquerda
                        Deslocamento.x -= 1.0f * 0.7071068f;
                        Deslocamento.y += 1.0f * 0.7071068f;
                        break;
                    case 4: //Mover para cima
                        Deslocamento.x = 0.0f;
                        Deslocamento.y += 1.0f;
                        break;
                    case 5: //Mover para cima direita
                        Deslocamento.x += 1.0f * 0.7071068f;
                        Deslocamento.y += 1.0f * 0.7071068f;
                        break;
                    case 6: //Mover para direita
                        Deslocamento.x += 1.0f;
                        Deslocamento.y = 0.0f;
                        break;
                    case 7: //Mover para direita baixo
                        Deslocamento.x += 1.0f * 0.7071068f;
                        Deslocamento.y -= 1.0f * 0.7071068f;
                        break;
                    default: //Ficar parado
                        Deslocamento.x = 0.0f;
                        Deslocamento.y = 0.0f;
                        break;
                }
            }
        }

        else if ( (Distancia <= RaioAtaqueLonge) && (Distancia > RaioAtaquePerto) )
        {
            //O jogador está a uma distância boa, mas ainda falta se aproximar para poder atacar
            //Debug.Log(gameObject.name + " está se aproximando do herói");
            //Deslocamento = Vector3.MoveTowards(Posicao, jogador.Posicao, Destreza);
            Deslocamento = (jogador.Posicao - Posicao).normalized;
        }
        else if (Distancia == RaioAtaquePerto)
        {
            //Este inimigo está na posição certa para atacar o herói.
            //Debug.Log(gameObject.name + " está na posição certa para atacar");
            //Fique parado e ataque
            if (TempoTimer > 0)
            {
                EmPosicaoPraAtacar = true;
                Deslocamento = Vector3.zero;
                TempoTimer -= Time.deltaTime;
            } else
            {
                EmPosicaoPraAtacar = false;
                //Deslocamento = Vector3.MoveTowards(Posicao, jogador.Posicao, Destreza);
                Deslocamento = (jogador.Posicao - Posicao).normalized;
                TempoTimer = TempoNaDirecao;
            }
        }
        else
        {
            //O inimigo está muito perto do herói.
            //Tente se afastar
            //Debug.Log(gameObject.name + " está se afastando do herói");
            //Verifique onde está o jogador
            if (jogador.Posicao.x > Posicao.x) //o jogador está à direita do inimigo
            {
                Deslocamento.x = transform.position.x - (RaioAtaquePerto * 0.6f);
            }
            if (jogador.Posicao.x < Posicao.x) //o jogador está à esquerda do inimigo
            {
                Deslocamento.x = transform.position.x + (RaioAtaquePerto * 0.6f);
            }
            if (jogador.Posicao.y > Posicao.y) //o jogador está acima do inimigo
            {
                Deslocamento.y = transform.position.y - (RaioAtaquePerto * 0.6f);
            }
            if (jogador.Posicao.x < Posicao.x) //o jogador está abaixo do inimigo
            {
                Deslocamento.y = transform.position.y + (RaioAtaquePerto * 0.6f);
            }
        }

        if ((!Mathf.Approximately(Deslocamento.x, 0.0f)) || (!Mathf.Approximately(Deslocamento.y, 0.0f)))
        {
            Direcao.Set(Deslocamento.x, Deslocamento.y, 0);
            Direcao.Normalize();
        }


        //Atualiza a variavel int da direcao do olhar do autor
        // 3-4-5
        // 2-X-6
        // 1-0-7
        if (Direcao.x == 0) //Olhando reto para cima ou para baixo 
        {
            if (Direcao.y < 0) DirecaoInt = 0; //Baixo
            else DirecaoInt = 4; //Cima
        }
        else
        {
            if (Direcao.x < 0) //Olhando para esquerda baixo, esquerda ou esquerda cima
            {
                if (Direcao.y < 0) DirecaoInt = 1; //esquerda baixo
                else
                {
                    if (Direcao.y == 0) DirecaoInt = 2; //esquerda
                    else DirecaoInt = 3; //esquerda cima
                }
            }
            else //Olhando para direita cima, direita ou direita baixo
            {
                if (Direcao.y > 0) DirecaoInt = 5; //direita cima
                else
                {
                    if (Direcao.y == 0) DirecaoInt = 6; //direita
                    else DirecaoInt = 7; //direita baixo
                }
            }
        }
        
        /*
        RaycastHit2D[] RaioVisao = Physics2D.RaycastAll(Posicao + Vector3.up * 0.6f, jogador.Posicao, RaioAtaqueLonge);
        //RaioVisao = Physics2D.Raycast(Posicao, jogador.Posicao, RaioAtaqueLonge, LayerMask.GetMask("Player"));
        /* Na verdade, não poderia ser na máscara de Layer Player.
         * Porque tem que lançar um raio e pegar todos os colisores dentro do RaioAtaqueLonge.
         * Se o primeiro que pegar "não" for o jogador, significa que acertou um obstáculo e, portanto,
         * não tem visão do jogador.
         
        if (RaioVisao[0].collider != null)
        {
            if (RaioVisao[0].collider.gameObject.tag != "Player")
            {
                Debug.Log("O Raycast acertou " + RaioVisao[0].collider + " e, portanto, não tem visão do herói");
            }
            else if (RaioVisao[0].collider.gameObject.tag == "Player")
            {
                Debug.Log("O Raycast acertou " + RaioVisao[0].collider + " e, portanto, tem visão do herói");
            }
        }
        */

        if (animador != null)
        {
            animador.SetFloat("Horizontal", Direcao.x);
            animador.SetFloat("Vertical", Direcao.y);
            animador.SetFloat("Velocidade", Deslocamento.magnitude);
        }

        Posicao.x = CorpoRigido.position.x + Deslocamento.x * Destreza * 0.5f * Time.fixedDeltaTime;
        Posicao.y = CorpoRigido.position.y + Deslocamento.y * Destreza * 0.5f * Time.fixedDeltaTime;

        CorpoRigido.MovePosition(Posicao);
    }

    public void Atualiza()
    {
        //Vamos rever todo esse esquema de "Recém Atacou" e "Folego". Por isso removi esses atributos do inimigo.
        //Me parece que o ideal é fazer esse controle pela própria skill, verificando o cooldown e se tem mana/stamina para executar..

        /*pode até ser, mas resolvi voltar com o recem atacou e o folego, pra fins de teste.
         * só outro detalhe: se formos verificar o cooldown por cada skill, supondo que o player ou o inimigo estava fora de
         * combate e estava com todas as skills recuperadas, assim que entrar em combate ele pode spammar todas as skills que quiser,
         * porque ele próprio não teria uma verificação de cooldown, só as skills.
         * Ou seja, eu acho que tem que ter os dois. Um cooldown separado para cada skill, bem como um cooldown do próprio jogador/inimigo,
         * pra não spammar todas as skills recuperadas.
         * */
        if (EmPosicaoPraAtacar && PodeAtacar)
        {
            UsarSkill(0);
            PodeAtacar = false;
        }
        if (!PodeAtacar)
        {
            folego -= Time.deltaTime;
        }
        if (folego <= 0)
        {
            folego = folegoMaximo;
            PodeAtacar = true;
        }
    }

    public void DisplayCriticalHit(int valor)
    {
        TextMeshProUGUI headerTextInstance = Instantiate(headerText, Vector3.up * 0.3f, Quaternion.identity);
        Transform canvasTransform = transform.Find("DynamicCanvas");
        headerTextInstance.transform.SetParent(canvasTransform, false);
        HeaderTextDisplay htd = headerTextInstance.GetComponent<HeaderTextDisplay>();
        htd.ConfigDisplayCriticalHit(valor.ToString());

        PontosDeVidaAtuais -= valor;
        if (PontosDeVidaAtuais >= PontosDeVidaTotais) PontosDeVidaAtuais = PontosDeVidaTotais;
        if (PontosDeVidaAtuais <= 0) Destroy(this.gameObject);
    }

    public void DisplayHit(int valor)
    {
        TextMeshProUGUI headerTextInstance = Instantiate(headerText, Vector3.up * 0.3f, Quaternion.identity);
        Transform canvasTransform = transform.Find("DynamicCanvas");
        headerTextInstance.transform.SetParent(canvasTransform, false);
        HeaderTextDisplay htd = headerTextInstance.GetComponent<HeaderTextDisplay>();
        htd.ConfigDisplayHit(valor.ToString());

        PontosDeVidaAtuais -= valor;
        if (PontosDeVidaAtuais >= PontosDeVidaTotais) PontosDeVidaAtuais = PontosDeVidaTotais;
        if (PontosDeVidaAtuais <= 0) Destroy(this.gameObject);
    }

    public void DisplayBlock()
    {
        TextMeshProUGUI headerTextInstance = Instantiate(headerText, Vector3.up * 0.35f, Quaternion.identity);
        Transform canvasTransform = transform.Find("DynamicCanvas");
        headerTextInstance.transform.SetParent(canvasTransform, false);
        HeaderTextDisplay htd = headerTextInstance.GetComponent<HeaderTextDisplay>();
        htd.ConfigDisplayBlock("BLOQUEIO!");
    }

    public void DisplayDodge()
    {
        TextMeshProUGUI headerTextInstance = Instantiate(headerText, Vector3.up * 0.35f, Quaternion.identity);
        Transform canvasTransform = transform.Find("DynamicCanvas");
        headerTextInstance.transform.SetParent(canvasTransform, false);
        HeaderTextDisplay htd = headerTextInstance.GetComponent<HeaderTextDisplay>();
        htd.ConfigDisplayDodge("ESQUIVA!");
    }

    public void UsarSkill(int skillIndex)
    {
        GameObject skill = skillsAtivas[skillIndex];
        SkillController_Inimigo SkillControllerInimigo = skill.GetComponent<SkillController_Inimigo>();
        SkillControllerInimigo.usuario = this.gameObject.GetComponent<Inimigo>();

        Vector3 lugarPraInstanciar = new Vector3();
        
        // 3-4-5
        // 2-X-6
        // 1-0-7
        lugarPraInstanciar = Posicao;
        switch (DirecaoInt)
        {
            case 0:
                lugarPraInstanciar.y -= SkillControllerInimigo.alcance;
                break;
            case 1:
                lugarPraInstanciar.x -= SkillControllerInimigo.alcance * 0.7071068f;
                lugarPraInstanciar.y -= SkillControllerInimigo.alcance * 0.7071068f;
                break;
            case 2:
                lugarPraInstanciar.x -= SkillControllerInimigo.alcance;
                break;
            case 3:
                lugarPraInstanciar.x -= SkillControllerInimigo.alcance * 0.7071068f;
                lugarPraInstanciar.y += SkillControllerInimigo.alcance * 0.7071068f;
                break;
            case 4:
                lugarPraInstanciar.y += SkillControllerInimigo.alcance;
                break;
            case 5:
                lugarPraInstanciar.x += SkillControllerInimigo.alcance * 0.7071068f;
                lugarPraInstanciar.y += SkillControllerInimigo.alcance * 0.7071068f;
                break;
            case 6:
                lugarPraInstanciar.x += SkillControllerInimigo.alcance;
                break;
            case 7:
                lugarPraInstanciar.x += SkillControllerInimigo.alcance * 0.7071068f;
                lugarPraInstanciar.y -= SkillControllerInimigo.alcance * 0.7071068f;
                break;
            default:
                break;
        }

        Instantiate(skill, lugarPraInstanciar, Quaternion.identity);
    }
}
