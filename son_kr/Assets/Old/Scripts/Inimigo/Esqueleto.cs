﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Esqueleto : Inimigo
{
    private void Start()
    {
        Nome = "Esqueleto";
        Forca = 1;
        Destreza = 1;
        Inteligencia = 1;

        PontosDeManaTotais = 0;
        PontosDeManaAtuais = 0;
        PontosDeVidaTotais = 20;
        PontosDeVidaAtuais = 20;

        RaioAtaquePerto = 1.0f;
        RaioAtaqueLonge = 2.0f;
        Posicao = transform.position;
    }
}
