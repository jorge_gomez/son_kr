﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using TMPro;
using UnityEngine;
using Pathfinding;

public class Inimigo_dummy : MonoBehaviour
{
    public string Nome;

    //Destreza
    [Header("Destreza")]
    public int Destreza;

    public Ator jogador;
   
    [Header("Combate")]
    public float RaioAtaquePerto = 1.0f;
    public float RaioAtaqueLonge = 3.0f;

    [Header("Física")]
    public Vector3 Posicao;
    private float TempoTimer;
    public float TempoNaDirecao = 2.0f;
    public float Distancia;
    
    [Header("Pathfinding")]
    public AIPath CaminhoDestino;
    public AIDestinationSetter SetadorDestino;
    public Transform TransformJogador;
    public GameObject TransformDestinoAleatorio;
    public Vector3 Destino;

    private void Start()
    {
        Posicao = transform.position;
        TempoTimer = TempoNaDirecao;
        CaminhoDestino = GetComponent<AIPath>();
        CaminhoDestino.canSearch = false;
        //CaminhoDestino.radius = RaioAtaqueLonge;
        CaminhoDestino.maxSpeed = Destreza;
        SetadorDestino = GetComponent<AIDestinationSetter>();
        //SetadorDestino.target = TransformDestinoAleatorio.transform;
        TransformJogador = FindObjectOfType<CharController>().GetTransform();
    }

    private void Update()
    {
        Movimenta();
    }

    public void Movimenta()
    {
        Distancia = Vector3.Distance(jogador.Posicao, Posicao);

        if (Distancia > RaioAtaqueLonge)
        {
            SetadorDestino.target = null;
            CaminhoDestino.canSearch = false;
            /*
            
            //Movimento aleatorio
            SetadorDestino.target = TransformDestinoAleatorio.transform;
            Destino = TransformDestinoAleatorio.transform.position;

            //Escolhe direção
            if (TempoTimer >= 0)
            {
                TempoTimer -= Time.deltaTime;
            }
            else
            {
                TempoTimer = TempoNaDirecao;
                int escolheDirecao = Random.Range(0, 8);
                switch (escolheDirecao)
                {
                    case 0: //Mover para baixo
                        //TransformDestinoAleatorio.Translate(TransformDestinoAleatorio.position.x - RaioAtaqueLonge, 0, 0);
                        Destino.y -= RaioAtaqueLonge;
                        break;
                    case 1: //Mover para baixo esquerda
                        //TransformDestinoAleatorio.Translate(TransformDestinoAleatorio.position.x - RaioAtaqueLonge * 0.7071068f, TransformDestinoAleatorio.position.y - RaioAtaqueLonge * 0.7071068f, 0);
                        Destino.x -= RaioAtaqueLonge * 0.7071068f;
                        Destino.y -= RaioAtaqueLonge * 0.7071068f;
                        break;
                    case 2: //Mover para esquerda
                        //TransformDestinoAleatorio.Translate(TransformDestinoAleatorio.position.x - RaioAtaqueLonge, TransformDestinoAleatorio.position.y, 0);
                        Destino.x -= RaioAtaqueLonge;
                        break;
                    case 3: //Mover para cima esquerda
                        //TransformDestinoAleatorio.Translate(TransformDestinoAleatorio.position.x - RaioAtaqueLonge * 0.7071068f, TransformDestinoAleatorio.position.y + RaioAtaqueLonge * 0.7071068f, 0);
                        Destino.x -= RaioAtaqueLonge * 0.7071068f;
                        Destino.y -= RaioAtaqueLonge * 0.7071068f;
                        break;
                    case 4: //Mover para cima
                        //TransformDestinoAleatorio.Translate(TransformDestinoAleatorio.position.x + RaioAtaqueLonge, 0, 0);
                        Destino.y += RaioAtaqueLonge;
                        break;
                    case 5: //Mover para cima direita
                        //TransformDestinoAleatorio.Translate(TransformDestinoAleatorio.position.x + RaioAtaqueLonge * 0.7071068f, TransformDestinoAleatorio.position.y + RaioAtaqueLonge * 0.7071068f, 0);
                        Destino.x += RaioAtaqueLonge * 0.7071068f;
                        Destino.y += RaioAtaqueLonge * 0.7071068f;
                        break;
                    case 6: //Mover para direita
                        //TransformDestinoAleatorio.Translate(0, TransformDestinoAleatorio.position.y + RaioAtaqueLonge, 0);
                        Destino.x += RaioAtaqueLonge;
                        break;
                    case 7: //Mover para direita baixo
                        //TransformDestinoAleatorio.Translate(TransformDestinoAleatorio.position.x + RaioAtaqueLonge * 0.7071068f, TransformDestinoAleatorio.position.y - RaioAtaqueLonge * 0.7071068f, 0);
                        Destino.x += RaioAtaqueLonge * 0.7071068f;
                        Destino.y -= RaioAtaqueLonge * 0.7071068f;
                        break;
                    default: //Ficar parado
                        break;
                }

                TransformDestinoAleatorio.transform.position = Vector3.MoveTowards(TransformDestinoAleatorio.transform.position, Destino, Time.deltaTime);
            }

            */

        } else
        {
            //Segue herói usando Pathfinding
            SetadorDestino.target = TransformJogador;
            CaminhoDestino.canSearch = true;
        }
    }

}
