﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HeaderTextDisplay : MonoBehaviour
{
    private TextMeshProUGUI textObj;

    private bool displayHit = false;
    private bool displayCriticalHit = false;
    private bool displayBlock = false;
    private bool displayDodge = false;
    private bool displayLife = false;

    private string textToDisplay;

    //=============================================================
    // Funções de configuração de display
    //
    // Essas funções de configuração são executadas antes do Start.
    // Ou seja, instancio um TMP, uso uma função de configuração e no próximo laço é que o Start vai ser executado..
    // Obs.: Esse é um comportamento comum e desejado do Unity
    //=============================================================

    public void ConfigDisplayLife(string value)
    {
        this.displayLife = true;
        this.textToDisplay = "+" + value;
    }

    public void ConfigDisplayHit(string value)
    {
        this.displayHit = true;
        this.textToDisplay = "-" + value;
    }

    public void ConfigDisplayCriticalHit(string value)
    {
        this.displayCriticalHit = true;
        this.textToDisplay = value;
    }

    public void ConfigDisplayBlock(string value)
    {
        this.displayBlock = true;
        this.textToDisplay = value;
    }

    public void ConfigDisplayDodge(string value)
    {
        this.displayDodge = true;
        this.textToDisplay = value;
    }

    //=============================================================

    void Start()
    {
        textObj = GetComponent<TextMeshProUGUI>();
        textObj.fontSize = 0.0f;
        textObj.text = textToDisplay;

        if (displayHit)
        {
            DisplayHit();
        } else
        if (displayCriticalHit)
        {
            DisplayCriticalHit();
        } else 
        if (displayBlock)
        {
            DisplayBlock();
        } else
        if (displayDodge)
        {
            DisplayDodge();
        }
        else
        if (displayLife)
        {
            DisplayLife();
        }
    }

    //=============================================================

    private void DisplayDodge()
    {
        UpdateColor(new Color(0.7924528f, 0.6463991f, 0.2280171f, 1.0f));
        textObj.text = textToDisplay;

        var fromFontSize = 0.0f;
        var toFontSize = 0.15f;

        LeanTween.value(this.gameObject, UpdateSize, fromFontSize, toFontSize, 0.2f).setEase(LeanTweenType.easeInOutBack).setOnComplete(() => {
            var fromColor = new Color(0.7924528f, 0.6463991f, 0.2280171f, 1.0f);
            var toColor = new Color(0.7924528f, 0.6463991f, 0.2280171f, 0.0f);

            LeanTween.value(this.gameObject, UpdateColor, fromColor, toColor, 0.4f).setEase(LeanTweenType.easeInQuad).setOnComplete(Destroi);
        });
    }

    private void DisplayBlock()
    {
        UpdateColor(new Color(0.7924528f, 0.6463991f, 0.2280171f, 1.0f));
        textObj.text = textToDisplay;

        var fromFontSize = 0.0f;
        var toFontSize = 0.15f;

        LeanTween.value(this.gameObject, UpdateSize, fromFontSize, toFontSize, 0.2f).setEase(LeanTweenType.easeInOutBack).setOnComplete(() => {
            var fromColor = new Color(0.7924528f, 0.6463991f, 0.2280171f, 1.0f);
            var toColor = new Color(0.7924528f, 0.6463991f, 0.2280171f, 0.0f);

            LeanTween.value(this.gameObject, UpdateColor, fromColor, toColor, 0.4f).setEase(LeanTweenType.easeInQuad).setOnComplete(Destroi);
        });
    }

    private void DisplayLife()
    {
        UpdateColor(new Color(0.0f, 1.0f, 0.0f, 1.0f));
        textObj.text = textToDisplay;

        var fromFontSize = 0.0f;
        var toFontSize   = 0.15f;

        LeanTween.value(this.gameObject, UpdateSize, fromFontSize, toFontSize, 0.5f).setEase(LeanTweenType.easeInOutBack).setOnComplete(() => {
            var fromColor = new Color(0.0f, 1.0f, 0.0f, 1.0f);
            var toColor   = new Color(0.0f, 1.0f, 0.0f, 0.0f);

            LeanTween.value(this.gameObject, UpdateColor, fromColor, toColor, 0.5f).setEase(LeanTweenType.easeInQuad).setOnComplete(Destroi);
        });
    }

    private void DisplayHit()
    {
        UpdateColor(new Color(1.0f, 0.0f, 0.0f, 1.0f));
        textObj.text = textToDisplay;

        var fromFontSize = 0.0f;
        var toFontSize = 0.15f;

        LeanTween.value(this.gameObject, UpdateSize, fromFontSize, toFontSize, 0.5f).setEase(LeanTweenType.easeInOutBack).setOnComplete(() => {
            var fromColor = new Color(1.0f, 0.0f, 0.0f, 1.0f);
            var toColor = new Color(1.0f, 0.0f, 0.0f, 0.0f);

            LeanTween.value(this.gameObject, UpdateColor, fromColor, toColor, 0.5f).setEase(LeanTweenType.easeInQuad).setOnComplete(Destroi);
        });
    }

    private void DisplayCriticalHit()
    {
        UpdateColor(new Color(1.0f, 0.0f, 0.0f, 1.0f));
        textObj.text = textToDisplay;

        var fromFontSize = 0.0f;
        var toFontSize = 0.3f;

        LeanTween.value(this.gameObject, UpdateSize, fromFontSize, toFontSize, 0.2f).setEase(LeanTweenType.easeInOutBack).setOnComplete(() => {
            var fromColor = new Color(1.0f, 0.0f, 0.0f, 1.0f);
            var toColor = new Color(1.0f, 0.0f, 0.0f, 0.0f);

            LeanTween.value(this.gameObject, UpdateColor, fromColor, toColor, 0.4f).setEase(LeanTweenType.easeInQuad).setOnComplete(Destroi);
        });
    }

    void UpdateColor(Color val)
    {
        textObj.color = val;
    }

    void UpdateSize(float val)
    {
        textObj.fontSize = val;
    }

    private void Destroi()
    {
        Destroy(this.gameObject);
    }
}
