﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LevelManager : MonoBehaviour
{
    [SerializeField]
    private Transform _map;
    [SerializeField]
    private Texture2D[] _mapData;
    [SerializeField]
    private MapElement[] _mapElements;
    [SerializeField]
    private Sprite _defaultTile;

    public Vector3 worldStartPosition
    {
        get
        {
            return Camera.main.ScreenToWorldPoint(new Vector3(0, 0));
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        GenerateMap();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void GenerateMap()
    {
        int height = _mapData[0].height;
        int width = _mapData[0].width;

        for (int i = 0; i < _mapData.Length; i++)
        {
            
            for (int x = 0; x < _mapData[i].width; x++)
            {
                for (int y = 0; y < _mapData[i].height; y++)
                {
                    Color c = _mapData[i].GetPixel(x, y);

                    MapElement element = Array.Find(_mapElements, e => e.color == c);

                    if (element != null)
                    {
                        float xPos = worldStartPosition.x + (_defaultTile.bounds.size.x * x);
                        float yPos = worldStartPosition.y + (_defaultTile.bounds.size.y * y);

                        GameObject go = Instantiate(element.elementPrefab);
                        if (element.tileTag == "Tree")
                        {
                            //go.GetComponent<SpriteRenderer>().sortingOrder = height*2 - y*2;
                            go.GetComponent<SpriteRenderer>().sortingOrder = 1;
                        }
                        go.transform.position = new Vector3(xPos, yPos);
                        go.transform.parent = _map;

                    }

                }
            }
        }
    }

}

[Serializable]
public class MapElement
{
    [SerializeField]
    private string _tileTag;
    [SerializeField]
    private Color _color;
    [SerializeField]
    private GameObject _elementPrefab;

    public string tileTag { get => _tileTag; }
    public Color color { get => _color; }
    public GameObject elementPrefab { get => _elementPrefab; }
    
}