﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Stat : MonoBehaviour
{
    //==========================================================================================
    //= Atributos
    //==========================================================================================

    private Image content;

    private float maxValue;
    private float currentValue;
    private float currentFill;

    [SerializeField]
    private Text textValue;

    //==========================================================================================
    //= Getters e Setters (Propriedades)
    //==========================================================================================

    public float CurrentValue
    {
        get {
            return currentValue;
        }
        set { 
            if (value > maxValue)
            {
                currentValue = maxValue;
            }
            else if (value < 0)
            {
                currentValue = 0f;
            }
            else
            {
                currentValue = value;
            }
            currentFill = currentValue / maxValue;

            if (textValue != null)
            {
                textValue.text = currentValue + "/" + maxValue;
            }
        }
    }

    public float MaxValue
    {
        get
        {
            return maxValue;
        }
        set
        {
            maxValue = value;
        }
    }


    //==========================================================================================
    //= Métodos
    //==========================================================================================

    // Start is called before the first frame update
    void Start()
    {
        content = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        if (currentFill != content.fillAmount)
        {
            content.fillAmount = Mathf.Lerp(content.fillAmount, currentFill, Time.deltaTime * 2);
        }
    }

    public void Initialize(float cValue, float mValue)
    {
        MaxValue = mValue;
        CurrentValue = cValue;
        if (content == null)
        {
            content = GetComponent<Image>();
        }
        content.fillAmount = CurrentValue / MaxValue;
    }
}
