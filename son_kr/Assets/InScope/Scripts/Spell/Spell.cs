﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Spell
{
    [SerializeField]
    private string _name;
    [SerializeField]
    private int _damage;
    [SerializeField]
    private Sprite _icon;
    [SerializeField]
    private float _speed;
    [SerializeField]
    private float _castTime;
    [SerializeField]
    private GameObject _prefab;
    [SerializeField]
    private Color _barColor;

    public string name { get => _name;}
    public int damage { get => _damage;}
    public Sprite icon { get => _icon;}
    public float speed { get => _speed;}
    public float castTime { get => _castTime;}
    public GameObject prefab { get => _prefab;}
    public Color barColor { get => _barColor;}
}
