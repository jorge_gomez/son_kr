﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class SpellScript : MonoBehaviour
{
    private Rigidbody2D rigidbody;
    private int damage;

    void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Launch(Vector2 direction, int damage)
    {
        this.damage = damage;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        rigidbody.AddForce(direction * 200);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "HitBox")
        {
            collision.GetComponentInParent<Enemy>().TakeDamage(this.damage);
            MyDestroy();
        }
    }

    void MyDestroy()
    {
        GetComponent<Animator>().SetTrigger("Impact");
        rigidbody.velocity = Vector2.zero;
    }

}
