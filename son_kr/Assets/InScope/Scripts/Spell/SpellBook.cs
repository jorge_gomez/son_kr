﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpellBook : MonoBehaviour
{
    [SerializeField]
    private Image _castingBar;

    [SerializeField]
    private Image _icon;

    [SerializeField]
    private Text _spellName;

    [SerializeField]
    private Text _castTime;

    [SerializeField]
    private Spell[] spells;

    [SerializeField]
    private CanvasGroup _canvasGroup;

    private Coroutine spellRoutine;
    private Coroutine fadeRoutine;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public Spell CastSpell(int spellIndex)
    {
        Spell s = spells[spellIndex];

        _castingBar.fillAmount = 0;
        _spellName.text = s.name;
        _castingBar.color = s.barColor;
        _icon.sprite = s.icon;

        spellRoutine = StartCoroutine(Progress(spellIndex));
        fadeRoutine = StartCoroutine(FadeBar());

        return s;
    }

    private IEnumerator Progress(int index)
    {
        float timePassed = Time.deltaTime;
        float rate = 1.0f / spells[index].castTime;
        float progress = 0.0f;

        while (progress <= 1.0f)
        {
            _castingBar.fillAmount = Mathf.Lerp(0, 1, progress);

            float timeLeft = spells[index].castTime - timePassed;
            if (timeLeft < 0.0f)
            {
                timeLeft = 0.0f;
            }

            _castTime.text = (timeLeft).ToString("F2");

            progress += rate * Time.deltaTime;
            timePassed += Time.deltaTime;

            yield return null;
        }

        StopProgress();
    }

    private IEnumerator FadeBar()
    {
        float rate = 1.0f / 0.25f;
        float progress = 0.0f;

        while (progress <= 1.0f)
        {
            _canvasGroup.alpha = Mathf.Lerp(0, 1, progress);
            progress += rate * Time.deltaTime;
            yield return null;
        }
    }

    public void StopProgress()
    {
        if (fadeRoutine != null)
        {
            StopCoroutine(fadeRoutine);
            fadeRoutine = null;
            _canvasGroup.alpha = 0;
        }

        if (spellRoutine != null)
        {
            StopCoroutine(spellRoutine);
            spellRoutine = null;
        }
    }

}
