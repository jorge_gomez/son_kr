﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void HealthChanged(float health);
public delegate void CharacterRemoved();

public class NPC : Character
{
    public event HealthChanged healthChanged;
    public event CharacterRemoved characterRemoved;

    [SerializeField]
    private Sprite _portrait;
    public Sprite portrait
    {
        get
        {
            return _portrait;
        }
    }

    public virtual void DeSelect()
    {
        healthChanged -= new HealthChanged(UIManager.instance.UpdateTargetFrame);
        characterRemoved -= new CharacterRemoved(UIManager.instance.HideTargetFrame);
    }

    public virtual Transform Select()
    {
        return base.hitBox;
    }

    public void OnHealthChanged(float health)
    {
        if (healthChanged != null)
        {
            healthChanged(health);
        }
    }

    public void OnCharacterRemoved()
    {
        if (characterRemoved != null)
        {
            characterRemoved();
        }

        Destroy(gameObject);
    }

}
