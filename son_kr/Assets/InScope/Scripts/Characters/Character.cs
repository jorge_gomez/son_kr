﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (Rigidbody2D))]
[RequireComponent (typeof (Animator))]
public abstract class Character : MonoBehaviour {
    [SerializeField]
    private float speed;
    protected Vector2 lookDirection;
    protected Vector2 direction;
    protected Animator animator;
    protected Rigidbody2D rigidbody2;

    protected bool isAttacking;
    protected Coroutine attackRoutine;

    [SerializeField]
    protected Transform hitBox;

    [SerializeField]
    protected Stat _health;

    public Stat health {
        get {
            return _health;
        }
    }

    [SerializeField]
    private int initialHealth;

    //==========================================================================================
    //= Getters e Setters (Propriedades)
    //==========================================================================================

    public bool IsMoving {
        get {
            return direction.x != 0 || direction.y != 0;
        }
    }

    //==========================================================================================
    //= Métodos
    //==========================================================================================

    // Start is called before the first frame update
    protected virtual void Start () {
        animator = GetComponent<Animator> ();
        rigidbody2 = GetComponent<Rigidbody2D> ();
        isAttacking = false;
        lookDirection = Vector2.down;

        _health.Initialize (initialHealth, initialHealth);
    }

    // Update is called once per frame
    protected virtual void Update () {
        AnimateMovement ();
    }

    private void FixedUpdate () {
        Move ();
    }

    public void Move () {
        if (speed > 0) {
            rigidbody2.velocity = direction.normalized * speed;
        }
    }

    public void AnimateMovement () {
        if (animator == null) //Check for controller reference
        {
            Debug.LogError ("Não pegou o animator");
        } else {
            if (IsMoving) {
                ActivateLayer ("WalkLayer");
                animator.SetFloat ("x", direction.x);
                animator.SetFloat ("y", direction.y);

                SetAttack (false);
            } else if (isAttacking) {
                ActivateLayer ("AttackLayer");
            } else {
                ActivateLayer ("IdleLayer");
            }
        }

    }

    public void ActivateLayer (string layerName) {
        for (int i = 0; i < animator.layerCount; i++) {
            animator.SetLayerWeight (i, 0);
        }

        animator.SetLayerWeight (animator.GetLayerIndex (layerName), 1);
    }

    public virtual void SetAttack (bool attackState) {
        if (attackState == false && attackRoutine != null) {
            StopCoroutine (attackRoutine);
        }

        isAttacking = attackState;
        animator.SetBool ("attack", attackState);
    }

    public virtual void TakeDamage (float damage) {
        _health.CurrentValue -= damage;

        if (_health.CurrentValue <= 0) {
            animator.SetTrigger ("die");
        }
    }

}