﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Threading;
using UnityEngine;

public class Player : Character
{
    [SerializeField]
    private Stat mana;
     
    [SerializeField]
    private Transform[] exits;
    private int exitIndex;

    private SpellBook spellBook;

    private Transform _target;
    public Transform target
    {
        get
        {
            return _target;
        }

        set
        {
            _target = value;
        }
    }

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        spellBook = GetComponent<SpellBook>();
        mana.Initialize(50, 50);
        exitIndex = 2;
    }

    // Update is called once per frame
    protected override void Update()
    {
        GetInput();

        base.Update();
    }

    public void GetInput()
    {
        direction = Vector2.zero;

        if (Input.GetKey(KeyCode.I))
        {
            health.CurrentValue -= 5;
            mana.CurrentValue -= 5;
        }
        if (Input.GetKey(KeyCode.O))
        {
            health.CurrentValue += 5;
            mana.CurrentValue += 5;
        }

        if (Input.GetKey(KeyCode.W))
        {
            direction += Vector2.up;
            lookDirection = Vector2.up;
            exitIndex = 0; 
        }
        if (Input.GetKey(KeyCode.A))
        {
            direction += Vector2.left;
            lookDirection = Vector2.left;
            exitIndex = 3;
        }
        if (Input.GetKey(KeyCode.S))
        {
            direction += Vector2.down;
            lookDirection = Vector2.down;
            exitIndex = 2;
        }
        if (Input.GetKey(KeyCode.D))
        {
            direction += Vector2.right;
            lookDirection = Vector2.right;
            exitIndex = 1;
        }
     }

    private IEnumerator Attack(int spellIndex)
    {
        SetAttack(true);

        Spell spell = spellBook.CastSpell(spellIndex);

        yield return new WaitForSeconds(spell.castTime);

        GameObject spellObject = Instantiate(spell.prefab, exits[exitIndex].position, Quaternion.identity);
        SpellScript spellController = spellObject.GetComponent<SpellScript>();
        spellController.Launch(lookDirection, spell.damage);

        SetAttack(false);
    }

    public void CastSpell(int spellIndex)
    {
        if (!isAttacking && !IsMoving)
        {
            attackRoutine = StartCoroutine(Attack(spellIndex));
        }
    }

    public override void SetAttack(bool attackState)
    {
        if (!attackState)
        {
            spellBook.StopProgress();
        }

        base.SetAttack(attackState);
    }

}
