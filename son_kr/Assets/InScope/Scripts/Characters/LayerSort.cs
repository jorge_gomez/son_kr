﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LayerSort : MonoBehaviour
{
    private SpriteRenderer parentRenderer;

    private List<Obstacle> obstacles = new List<Obstacle>();

    // Start is called before the first frame update
    void Start()
    {
        parentRenderer = transform.parent.GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Obstacle")
        {
            Obstacle o = collider.GetComponent<Obstacle>();
            o.FadeIn();

            /* Código abaixo fazia o controle da ordem de renderização da imagem, mas não precisa porque essa versão do Unity já faz de acordo com o eixo Y
            if (obstacles.Count == 0 || o.spriteRenderer.sortingOrder - 1 < parentRenderer.sortingOrder)
            {
                Transform colliderTransform = collider.GetComponent<Transform>();
                //Transform playerTransform = transform.parent.GetComponent<Transform>();
                Transform playerTransform = transform;

                //Vector3 colliderPivot = Camera.main.ScreenToWorldPoint(o.spriteRenderer.sprite.pivot);
                //Vector3 playerPivot = Camera.main.ScreenToWorldPoint(parentRenderer.sprite.pivot);

                //float playerY = playerTransform.position.y + playerPivot.y;
                //float colliderY = colliderTransform.position.y + colliderPivot.y;

                float playerY = playerTransform.position.y;
                float colliderY = colliderTransform.position.y;

                Debug.Log("Colidiu com " + collider + ". Sort do player: " + parentRenderer.sortingOrder + " -> Sort do obstáculo " + o.spriteRenderer.sortingOrder);
                Debug.Log("Colidiu com " + collider + ". Y do player: " + playerY + " -> Y do obstáculo " + colliderY);

                //Aqui na verdade é necessário conferir o y

                //parentRenderer.sortingOrder = o.spriteRenderer.sortingOrder - 1;

                Debug.Log("Sort do player foi para " + parentRenderer.sortingOrder + " -> Sort do obstáculo " + o.spriteRenderer.sortingOrder);
            }
            */

            obstacles.Add(o);
        }
    }

    private void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.tag == "Obstacle")
        {
            Obstacle o = collider.GetComponent<Obstacle>();
            o.FadeOut();
            obstacles.Remove(o);

            /* Código abaixo fazia o controle da ordem de renderização da imagem, mas não precisa porque essa versão do Unity já faz de acordo com o eixo Y
            if (obstacles.Count == 0)
            {
                Debug.Log("Saiu de todos os colisores: volta para 200");
                //parentRenderer.sortingOrder = 200;
            } else
            {
                obstacles.Sort();
                //parentRenderer.sortingOrder = obstacles[0].spriteRenderer.sortingOrder - 1;
                Debug.Log("Ainda tem colisor. Sort do player: " + parentRenderer.sortingOrder + " -> Sort do obstáculo restante: " + obstacles[0].spriteRenderer.sortingOrder);
            }
            */
        }
            
    }

}
