﻿using System.Collections;
using System.Collections.Generic;
using System.IO.Pipes;
using UnityEngine;
using UnityEngine.EventSystems;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private Player player;

    [SerializeField]
    private NPC currentTarget;

    // Update is called once per frame
    void Update()
    {
        ClickTarget();
    }

    private void ClickTarget()
    {
        if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero, Mathf.Infinity, 1);

            if (hit.collider != null)
            {
                //Dá para verificar ainda pelas tags com hit.collider.tag == "NomeDaTag"
                //Por exemplo, só seleciona se tiver a tag "Inimigo"
                if (currentTarget != null)
                {
                    currentTarget.DeSelect();
                }

                currentTarget = hit.collider.GetComponent<NPC>();

                player.target = currentTarget.Select();

                UIManager.instance.ShowTargetFrame(currentTarget);

            } else
            {
                if (currentTarget != null)
                {
                    currentTarget.DeSelect();
                }

                currentTarget = null;
                player.target = null;

                UIManager.instance.HideTargetFrame();
            }
        }
    }
}
