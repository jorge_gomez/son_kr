﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WIPGameManager : MonoBehaviour {
    #region Singleton
    private static WIPGameManager _instance;
    public static WIPGameManager Instance {
        get {
            if (_instance == null) {
                _instance = GameObject.FindObjectOfType<WIPGameManager> ();
            }

            return _instance;
        }
    }
    #endregion

    //Singletons are a lazy way but efficient way to make public calls -->WIPGamemanager.instance.etc...
    void Awake () {
        DontDestroyOnLoad (gameObject);
        if (Player == null) {
            Player = transform.Find ("Player").gameObject;
        }
        if (PController == null) {
            PController = Player.gameObject.GetComponent<WIPPlayerController> ();
        }
    }

    [Header ("PlayerRelated")]
    public GameObject Player;
    public WIPInput Input;
    public WIPPlayerController PController;

}