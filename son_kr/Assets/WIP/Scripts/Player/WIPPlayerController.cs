﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WIPPlayerController : WIPCharacter {

    [Header ("Transforms")]
    [SerializeField]
    private Transform _target;
    public Transform target { get { return _target; } set { _target = value; Debug.Log ("Target: " + value); } }

    [SerializeField]
    private Transform exit;
    private int exitIndex;

    //
    [Header ("Atributes")]
    [SerializeField]
    private Stat mana;
    private SpellBook spellBook;

    //
    protected override void Start () {
        base.Start ();
        spellBook = GetComponent<SpellBook> ();
        mana.Initialize (50, 50);
        exitIndex = 2;
    }
    private void Awake () {

    }
    protected override void Update () {
        base.Update ();
    }

    //==========================================================================================
    //= Input, shooting and Movement
    //==========================================================================================

    public void MoveInput (Vector2 _moveDir) {
        direction = _moveDir;
        if (_moveDir != new Vector2 (0, 0)) {
            lookDirection = _moveDir.normalized;
            return;
        }
    }

    //==========================================================================================
    //= Attack, Speels, etc...
    //==========================================================================================
    private IEnumerator Attack (int spellIndex) {
        SetAttack (true);

        Spell spell = spellBook.CastSpell (spellIndex);

        yield return new WaitForSeconds (spell.castTime);

        GameObject spellObject = Instantiate (spell.prefab, exit.position, Quaternion.identity);
        SpellScript spellController = spellObject.GetComponent<SpellScript> ();
        spellController.Launch (lookDirection, spell.damage);

        SetAttack (false);
    }

    public void CastSpell (int spellIndex) {
        if (!isAttacking && !IsMoving) {
            attackRoutine = StartCoroutine (Attack (spellIndex));
        }
    }

    public override void SetAttack (bool attackState) {
        if (!attackState) {
            spellBook.StopProgress ();
        }

        base.SetAttack (attackState);
    }

}