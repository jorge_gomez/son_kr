﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WIPInput : MonoBehaviour {

    [Header ("Input")]
    [SerializeField]
    private Joystick walkJoy, shootJoy;
    private Vector2 walkDir, shootDir;
    private int exit;

    private void Update () {
        GetMoveInput ();
    }

    private void GetMoveInput () {
        Vector2 dir = new Vector2 (walkJoy.Horizontal, walkJoy.Vertical);
        WIPGameManager.Instance.PController.MoveInput (dir);
    }
    private void GetShootInput () {
        
    }

}